// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebaseConfig:  {
    apiKey: "AIzaSyBEKcNEDScjrLqIQ2ata--R71Go6QaWuFY",
    authDomain: "app-slon.firebaseapp.com",
    databaseURL: "https://app-slon.firebaseio.com",
    projectId: "app-slon",
    storageBucket: "app-slon.appspot.com",
    messagingSenderId: "729985655541",
    appId: "1:729985655541:web:be0b7f6a13254b9073dbbc",
    measurementId: "G-3KWETXYBGS"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
