import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/shared/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private login: LoginService, public router: Router) {
    
   }

  ngOnInit(): void {
  }

  public logOut() {
    this.login.logOut()
    .then((data) => {
      this.router.navigate(['login']);
    });
  }

}
