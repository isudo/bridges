import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { AuthGuard } from '../shared/services/auth-guard.service';

const dashboardRoutes: Routes = [
  { path: '', canActivate: [AuthGuard],  component: DashboardComponent , pathMatch: 'full'},
    
];
@NgModule({
  imports: [
    RouterModule.forChild(dashboardRoutes)],
  exports: [RouterModule]
})
export class DasboardRoutingModule { }
