import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard.component';
import { HeaderComponent } from '../dashboard/header/header.component';
import { FooterComponent } from '../dashboard/footer/footer.component';
import { DesktopComponent } from './desktop/desktop.component';
import { DasboardRoutingModule } from './dashboard-routing.module';


@NgModule({
  declarations: [
    DesktopComponent,
    DashboardComponent,
    HeaderComponent,
    FooterComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    DasboardRoutingModule
  ],
  entryComponents:[
  ]
})
export class DashboardModule { }