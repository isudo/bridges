import {Component,OnInit} from '@angular/core';
import {AngularFireStorage,AngularFireStorageReference,AngularFireUploadTask} from '@angular/fire/storage';
import * as firebase from 'firebase';
import {AngularFireAuth} from '@angular/fire/auth';
import { DataImg } from 'src/app/shared/models/dataImg';

@Component({
	selector: 'app-desktop',
    templateUrl: './desktop.component.html',
 	styleUrls: ['./desktop.component.scss']
})
export class DesktopComponent implements OnInit {

    public ref: AngularFireStorageReference;
	public task: AngularFireUploadTask;
	public defaultBase: string = 'base/';
	public user: firebase.User;
	public dataBase  = [new DataImg()];

	constructor(public afAuth: AngularFireAuth, private afStorage: AngularFireStorage) {
	}

	public upload(event) {
		const id = event.target.files[0].name as string;
		const filePath = this.defaultBase + this.user.uid + '/' + id;
		this.ref = this.afStorage.ref(filePath);
		this.task = this.ref.put(event.target.files[0]);

		this.task.then(() => {
		this.showImg();
		});
	}

  	private showImg() {
		this.afAuth.auth.onAuthStateChanged((user) => {
			if (user) {
				this.user = user;
				const ref = firebase.storage().ref(this.defaultBase);
				
				ref.child(this.user.uid).listAll()
				.then(response => {
					this.parsingImg(response);
				})
			}
		});
	  }
	  
	private parsingImg(data) {
		data.items.forEach((items, i, arr) => {
			const tempData = new DataImg();

				arr[i].getDownloadURL()
				.then((resp) => {
					tempData.url = resp;
				})

				arr[i].getMetadata()
				.then((resp) => {
					tempData.name = resp.name;
					tempData.size = resp.size;
				})
			this.dataBase[i] = tempData;
		})
	}


	public random() {
		this.dataBase = this.dataBase.sort(function(){
			return Math.random() - 0.5;
		});
	}

	public sort(){

		this.dataBase = this.dataBase.sort((a, b)=>{
			let nameA = a.name.toLocaleLowerCase();
			let nameB = b.name.toLocaleLowerCase();
			if(nameA<nameB) {
				return -1;
			}
			if (nameA > nameB) {
				return 1
			}
			return 0
		});
		console.log(this.dataBase);
	}
	
	public size(){

		this.dataBase = this.dataBase.sort((a, b)=>{
			return (a.size - b.size);
		});
	}

	ngOnInit(): void {
		this.showImg();
	}
}
