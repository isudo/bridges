import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/shared/services/login.service';
import { ToastrService } from 'ngx-toastr';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    public loginForm: FormGroup;
    public signUpForm: FormGroup;
    public resetMailForm: FormGroup;
    public formLogin_active: boolean = true;
    public formSign_active: boolean = false;
    public formResetMail_active: boolean = false;
    public mailForReset: string;
    public errorRegistration: string;
    public errorLogIn: string;
    private patternMail: RegExp = /^[0-9a-z-\.]+\@[0-9a-z-]{2,}\.[a-z]{2,}$/i;

    constructor(
        public fb: FormBuilder, 
        public loginService: LoginService, 
        public router: Router, 
        private toastrService: ToastrService) {}

    ngOnInit(): void {
        this.buildForms();
    }

    // private buildLoginFrom(): void {
    //     this.loginForm = this.fb.group({
    //     login: ['', [Validators.required, Validators.pattern(this.patternMail)]],
    //     password: ['', [Validators.required, Validators.minLength(4)]],
    //     });
    // }

    // private buildSignUpForm(): void {
    //     this.signUpForm  = this.fb.group({
    //     login: ['', [Validators.required, Validators.pattern(this.patternMail)]],
    //     password: ['', [Validators.required, Validators.minLength(4)]],
    //     name: ['', [Validators.required, Validators.minLength(2)]],    
    //     });
    // }

    // private buildResetMailForm(): void {
    //     this.resetMailForm  = this.fb.group({
    //     mail: ['', [Validators.required, Validators.pattern(this.patternMail)]],
    //     });
    // }

    private buildForms(): void {

        this.loginForm = this.fb.group({
                login: ['', [Validators.required, Validators.pattern(this.patternMail)]],
                password: ['', [Validators.required, Validators.minLength(4)]],
            });

        this.signUpForm  = this.fb.group({
                login: ['', [Validators.required, Validators.pattern(this.patternMail)]],
                password: ['', [Validators.required, Validators.minLength(4)]],
                name: ['', [Validators.required, Validators.minLength(2)]],    
            });

        this.resetMailForm  = this.fb.group({
                mail: ['', [Validators.required, Validators.pattern(this.patternMail)]],
            });
    }

    public formLoginShow():void {
        this.formLogin_active = true;
        this.formSign_active = false;
        this.formResetMail_active = false;
    }

    public formSignShow():void {
        this.formSign_active = true;
        this.formLogin_active = false;
        this.formResetMail_active = false;
    }

    public showRestPasswordForm(): void {
        this.formResetMail_active = true;
        this.formLogin_active = false;
        this.formSign_active = false;
    }

    public GetRestPasswordForm(): void {
        this.loginService.restPassword(this.mailForReset)
            .then(
                ()=>{
                this.formLoginShow(); // почему срабатывает после cath LoginServise 35-line
                }
            )
        }

    public login(): void {
        const { login , password } = this.loginForm.value;
        this.loginService
            .login(login, password)
            .then((data) => {
                this.router.navigate(['dashboard']);
                this.toastrService.success('Welcome to your office');
            })
            .catch((error) => {
            this.toastrService.error(error.message);
            this.errorLogIn = error.message;
            console.log(error.message);

            });
        };

    public signUp(): void {
        console.log('signUp')
        const{login, password, name} = this.signUpForm.value;

        this.loginService.signUp(login, password, name)
            .then((data: firebase.auth.UserCredential)  => {  // : Promise<firebase.auth.UserCredential> ????????
                console.log('ok lets do it')
                data.user.sendEmailVerification();
                this.formLoginShow();
                return data;
            })
            .then((data: firebase.auth.UserCredential) => {
                console.log('return this.loginService.addUser')
                return this.loginService.addUser({name, email: login}, data.user.uid)
                
            })
            .catch((error) => {
                console.log(error);
                this.errorRegistration = error.message;
            });
    };

}
