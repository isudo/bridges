import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection, DocumentSnapshot } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { User } from '../models/user';
import { BehaviorSubject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';


@Injectable({
    providedIn: 'root'
})
export class LoginService {

    private usersCollection: AngularFirestoreCollection<User>;
    public user: firebase.User;
    public userFromDb: BehaviorSubject<User> = new BehaviorSubject(null);

    constructor(
        public afAuth: AngularFireAuth, 
        private afs: AngularFirestore, 
        private toastrService: ToastrService) {

        this.afAuth.auth.onAuthStateChanged((user) => {
            if (user) {
                this.user = user;
                console.log(user.email)
            }
        });
    }

    public restPassword(mail: string){
        return this.afAuth.auth.sendPasswordResetEmail(mail)
            .then(()=>{
                this.toastrService.success('send Rest Mail');
            })
            .catch((error)=>{
                this.toastrService.error(error.message);
                console.log('something wrong');
            })
    }

    public login(login: string, password: string): Promise<firebase.auth.UserCredential> {
        console.log('Persistence.LOCAL', firebase.auth.Auth.Persistence.LOCAL);

        return this.afAuth.auth.setPersistence(firebase.auth.Auth.Persistence.LOCAL)
            .then((data) => {
                console.log('data', data);
                return this.afAuth.auth.signInWithEmailAndPassword(login, password);
            });
    }

    public signUp(login: string, password: string, name: string) {
        return this.afAuth.auth.createUserWithEmailAndPassword(login, password)

    }

    public addUser(data: any, uid: string): Promise<any> {
        this.usersCollection = this.afs.collection<User>('user');
        this.toastrService.success('registration success');

        return this.usersCollection.doc(uid).set(data);
    }

    public logOut() {
        return this.afAuth.auth.signOut();
    }

    public getAuth() {
        console.log('this.afAuth.authState', this.afAuth.authState);
        return  this.afAuth.authState;
    }

    get authenticated(): boolean {
        console.log('this.user', this.user);
        return !!this.user;
    }

    // public updateUser(data: User) {
    //     return this.afs.collection('user', ref => ref.where('email', '==', data.email))
    //         .doc(this.user.uid).update(data);
    // }

}
