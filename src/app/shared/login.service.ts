import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection, DocumentSnapshot } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { User } from './user';
import { BehaviorSubject } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private usersCollection: AngularFirestoreCollection<User>;
  public user: firebase.User;
  public userFromDb: BehaviorSubject<User> = new BehaviorSubject(null);

  constructor(public afAuth: AngularFireAuth, private afs: AngularFirestore) {

    this.afAuth.auth.onAuthStateChanged((user) => {
          console.log('user is logged', user);
          if (user) {
              this.user = user;
              console.log(user.email)
          }
    });
  }

  public login(login: string, password: string): Promise<firebase.auth.UserCredential> {
    console.log('Persistence.LOCAL', firebase.auth.Auth.Persistence.LOCAL);

    return this.afAuth.auth.setPersistence(firebase.auth.Auth.Persistence.LOCAL)
    .then((data) => {
        console.log('data', data);
        return this.afAuth.auth.signInWithEmailAndPassword(login, password);
    });
  }

  public signUp(login: string, password: string, name: string) {
    return this.afAuth.auth.createUserWithEmailAndPassword(login, password);

  }

  public addUser(data: any, uid: string): Promise<any> {
    console.log('data', data, 'uid', uid);
    this.usersCollection = this.afs.collection<User>('user');
    console.log(this.usersCollection);
    return this.usersCollection.doc(uid).set(data);
  }

  public logOut() {
    return this.afAuth.auth.signOut();
  }

  public getAuth() {
    return  this.afAuth.authState;
  }

  get authenticated(): boolean {
    return !!this.user;
  }

  public updateUser(data: User) {
    return this.afs.collection('user', ref => ref.where('email', '==', data.email)).doc(this.user.uid).update(data);
  }

}
