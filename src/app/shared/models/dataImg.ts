export class DataImg {
    name: string = '';
    url: string = '';
    size: number = 0;
}